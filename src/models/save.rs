use std::{collections::HashMap, fs, io::Cursor};

use crate::{
    error::SaveError,
    parsing::{
        compression::decompress_body, parser::SaveParser, reader::SaveReader,
    },
};

use super::{
    buildings::miner::Miner,
    resources::{deposit::ResourceDeposit, node::ResourceNode},
    save_header::SaveHeader,
};

#[derive(Debug, Clone)]
pub struct Save {
    pub resource_deposits: HashMap<String, ResourceDeposit>,
    pub resource_nodes: HashMap<String, ResourceNode>,
    pub miners: HashMap<String, Miner>,
}

impl Save {
    pub fn from_file(filename: &str) -> Result<Save, SaveError> {
        let data = fs::read(filename)?;

        Self::from_bytes(data)
    }

    pub fn from_bytes(bytes: Vec<u8>) -> Result<Save, SaveError> {
        let mut bytes_stream = Cursor::new(bytes);

        let header = SaveHeader::from_stream(&mut bytes_stream)?;

        let decompressed_bytes = decompress_body(&mut bytes_stream, &header)?;

        let mut stream = Cursor::new(decompressed_bytes);

        let mut save_file_reader = SaveReader::new(&mut stream, header);

        save_file_reader.read()?;

        let save_file_parser = SaveParser::from(save_file_reader);

        save_file_parser.parse()
    }
}
