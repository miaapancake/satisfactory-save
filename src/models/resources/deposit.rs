use crate::models::entity::Entity;
use crate::models::resources::ResourceType;
use crate::models::{Quat, Vector};

const RESOURCE_DEPOSIT_CLASSES: [&str; 1] =
    ["/Game/FactoryGame/Resource/BP_ResourceDeposit.BP_ResourceDeposit_C"];

#[derive(Clone, Debug)]
pub struct ResourceDeposit {
    pub name: String,
    pub location: Vector,
    pub rotation: Quat,
    pub resource: ResourceType,
    pub occupied: bool,
}

impl ResourceDeposit {
    pub fn new(entity: &Entity) -> Self {
        Self {
            name: entity.get_instance_name().clone(),
            resource: Self::get_resource_type(entity),
            location: entity.get_position().unwrap_or_default(),
            rotation: entity.get_rotation().unwrap_or_default(),
            occupied: false,
        }
    }

    pub fn get_resource_type(entity: &Entity) -> ResourceType {
        entity.get_property_int("mResourceDepositTableIndex").into()
    }

    pub fn is_resource_deposit(entity: &Entity) -> bool {
        RESOURCE_DEPOSIT_CLASSES.contains(&entity.get_type().as_str())
    }
}
