use std::collections::HashMap;

use serde::Deserialize;
use serde_json::from_str;

use crate::models::{entity::Entity, Quat, Vector};

use super::ResourceType;

const RESOURCE_NODE_CLASSES: [&str; 1] =
    ["/Game/FactoryGame/Resource/BP_ResourceNode.BP_ResourceNode_C"];

const NODE_INFO: &str = include_str!("../../../data/node_info.json");

pub fn get_node_info(
) -> Result<HashMap<String, ResourceInfo>, serde_json::Error> {
    from_str(NODE_INFO)
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Purity {
    Impure,
    Normal,
    Pure,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ResourceInfo {
    pub purity: Purity,
    pub resource: ResourceType,
}

#[derive(Debug, Clone)]
pub struct ResourceNode {
    pub purity: Purity,
    pub resource: ResourceType,
    pub location: Vector,
    pub rotation: Quat,
}

impl ResourceNode {
    pub fn new(entity: &Entity, info: ResourceInfo) -> Self {
        Self {
            purity: info.purity,
            resource: info.resource,
            location: entity.get_position().unwrap_or_default(),
            rotation: entity.get_rotation().unwrap_or_default(),
        }
    }

    pub fn is_resource_node(entity: &Entity) -> bool {
        RESOURCE_NODE_CLASSES.contains(&entity.get_type().as_str())
    }
}
