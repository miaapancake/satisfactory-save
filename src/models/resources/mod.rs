use serde::Deserialize;

pub mod deposit;
pub mod node;

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "snake_case")]
pub enum ResourceType {
    Limestone,
    Caterium,
    Stone,
    Iron,
    Copper,
    Coal,
    Gold,
    Sulfur,
    Oil,
    Quartz,
    Bauxite,
    #[serde(rename = "sam")]
    SAM,
    Uranium,
    #[serde(other)]
    Unknown,
}

impl From<Option<i32>> for ResourceType {
    fn from(value: Option<i32>) -> Self {
        match value {
            Some(0) => Self::Stone,
            Some(1) => Self::Iron,
            Some(2) => Self::Copper,
            Some(3) => Self::Coal,
            Some(4) => Self::Gold,
            Some(5) => Self::Sulfur,
            Some(6) => Self::Quartz,
            Some(7) => Self::Bauxite,
            Some(8) => Self::SAM,
            Some(9) => Self::Uranium,
            _ => Self::Unknown,
        }
    }
}
