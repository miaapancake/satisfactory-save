use log::warn;

use crate::{
    models::entity::Entity,
    models::{ObjectReference, Quat, Vector},
};

// Different save versions seem to have different capitalization
const MINER_CLASSES: [&str; 6] = [
    "/Game/FactoryGame/Buildable/Factory/MinerMK1/Build_MinerMk1.Build_MinerMk1_C",
    "/Game/FactoryGame/Buildable/Factory/MinerMK2/Build_MinerMk2.Build_MinerMk2_C",
    "/Game/FactoryGame/Buildable/Factory/MinerMK3/Build_MinerMk3.Build_MinerMk3_C",
    "/Game/FactoryGame/Buildable/Factory/MinerMk1/Build_MinerMk1.Build_MinerMk1_C",
    "/Game/FactoryGame/Buildable/Factory/MinerMk2/Build_MinerMk2.Build_MinerMk2_C",
    "/Game/FactoryGame/Buildable/Factory/MinerMk3/Build_MinerMk3.Build_MinerMk3_C"
];

#[derive(Debug, Clone)]
pub struct Miner {
    pub tier: i8,
    pub extracting: Option<ObjectReference>,
    pub position: Vector,
    pub rotation: Quat,
}

impl Miner {
    pub fn new(entity: &Entity) -> Self {
        let tier = match entity.get_type().split('/').last() {
            Some("Build_MinerMk1.Build_MinerMk1_C") => 1,
            Some("Build_MinerMk2.Build_MinerMk2_C") => 2,
            Some("Build_MinerMk3.Build_MinerMk3_C") => 3,
            other => {
                warn!("Unknown miner type: {:#?}", other);
                -1
            }
        };

        Self {
            position: entity.get_position().unwrap_or_default(),
            rotation: entity.get_rotation().unwrap_or_default(),
            tier,
            extracting: entity
                .get_property_object_reference("mExtractableResource"),
        }
    }

    pub fn is_miner(entity: &Entity) -> bool {
        MINER_CLASSES.contains(&entity.get_type().as_str())
    }
}
