use std::collections::HashMap;

use self::{
    property::{Property, TextProperty},
    struct_property::TypedData,
};

pub mod buildings;
pub mod entity;
pub mod level;
pub mod property;
pub mod resources;
pub mod save;
pub mod save_header;
pub mod struct_property;

#[derive(Clone, Debug)]
pub struct ObjectReference {
    pub level_name: String,
    pub path_name: String,
}

#[derive(Clone, Debug)]
pub enum Element {
    Unknown(Box<HashMap<String, Property>>),
    Byte(u8),
    Bool(bool),
    Enum(String),
    Str(String),
    Int(i32),
    UInt(u32),
    Int64(i64),
    Float(f32),
    Interface(ObjectReference),
    Object(ObjectReference),
    ArrayStruct(Vec<TypedData>),
    Text(TextProperty),
    Vector(Vector),
}

#[derive(Clone, Debug)]
pub struct Vector2D {
    pub x: f64,
    pub y: f64,
}

#[derive(Clone, Debug)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Default for Vector {
    fn default() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Quat {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub w: f64,
}

impl Default for Quat {
    fn default() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            z: 0.0,
            w: 0.0,
        }
    }
}
