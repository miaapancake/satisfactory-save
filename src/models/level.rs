use super::{entity::Entity, ObjectReference};

pub struct Level {
    pub sublevel_name: String,
    pub collectables: Vec<ObjectReference>,
    pub entities: Vec<Entity>,
}
