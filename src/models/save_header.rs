use std::io::{self, Read, Seek, SeekFrom};

use byteorder::{ReadBytesExt, LE};
use log::info;

use crate::{error::SaveError, parsing::*};

#[derive(Debug)]
pub struct SaveHeader {
    pub version: i32,
    pub save_version: i32,
    pub build_version: i32,
    pub map_name: String,
    pub map_options: String,
    pub session_name: String,
    pub played_seconds: i32,
    pub timestamp: i64,
    pub session_visibility: SessionVisibility,
    pub editor_object_version: i32,
    pub mod_metadata: String,
    pub mod_flags: i32,
    pub save_identifier: String,
    pub is_partioned: Option<bool>,
    pub creative: Option<bool>,
}

impl SaveHeader {
    pub fn from_stream<T>(mut value: T) -> Result<Self, io::Error>
    where
        T: Seek + Read,
    {
        let mut header = Self {
            version: value.read_i32::<LE>()?,
            save_version: value.read_i32::<LE>()?,
            build_version: value.read_i32::<LE>()?,
            map_name: read_string(&mut value)?,
            map_options: read_string(&mut value)?,
            session_name: read_string(&mut value)?,
            played_seconds: value.read_i32::<LE>()?,
            timestamp: read_long(&mut value)?,
            session_visibility: value.read_u8()?.try_into().unwrap(),
            editor_object_version: value.read_i32::<LE>()?,
            mod_metadata: read_string(&mut value)?,
            mod_flags: value.read_i32::<LE>()?,
            save_identifier: read_string(&mut value)?,
            is_partioned: None,
            creative: None,
        };

        if header.version >= 13 {
            header.is_partioned = Some(value.read_i32::<LE>()? != 0);
            value.seek(SeekFrom::Current(20))?; // skip save hash
            header.creative = Some(value.read_i32::<LE>()? != 0);
        }

        info!("Parsing Satisfactory Save Version: {}", header.save_version);

        Ok(header)
    }
}

#[derive(Debug)]
pub enum SessionVisibility {
    Private,
    FriendsOnly,
}

impl TryFrom<u8> for SessionVisibility {
    type Error = SaveError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Ok(match value {
            0 => Self::Private,
            1 => Self::FriendsOnly,
            _ => Err(SaveError::Parsing(
                "Invalid value found for SessionVisibility",
            ))?,
        })
    }
}

impl From<SessionVisibility> for i8 {
    fn from(value: SessionVisibility) -> Self {
        match value {
            SessionVisibility::Private => 0,
            SessionVisibility::FriendsOnly => 1,
        }
    }
}
