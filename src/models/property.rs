use std::collections::HashMap;

use crate::error::SaveError;

use super::{
    struct_property::{LinearColor, TypedData},
    Element, ObjectReference, Vector,
};

#[derive(Clone, Debug)]
pub enum Property {
    Array(Vec<Element>),
    NoneByte(u8),
    Byte(String),
    Enum(String),
    Bool(bool),
    Float(f32),
    Int(i32),
    UInt(u32),
    Int8(i8),
    Int64(i64),
    Double(f64),
    Map(HashMap<String, Element>),
    Set(Vec<Element>),
    Object(ObjectReference),
    Str(String),
    Struct(TypedData),
    Text(TextProperty),
}

impl Property {
    pub(crate) fn read_none_byte(&self) -> Result<u8, SaveError> {
        match self {
            Self::NoneByte(value) => Ok(*value),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non none byte property as none byte",
            )),
        }
    }

    pub(crate) fn read_int(&self) -> Result<i32, SaveError> {
        match self {
            Self::Int(value) => Ok(*value),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non int property as int",
            )),
        }
    }

    pub(crate) fn read_bool(&self) -> Result<bool, SaveError> {
        match self {
            Self::Bool(value) => Ok(*value),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non bool property as bool",
            )),
        }
    }

    pub(crate) fn read_double(&self) -> Result<f64, SaveError> {
        match self {
            Self::Double(value) => Ok(*value),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non double property as double",
            )),
        }
    }

    pub(crate) fn read_float(&self) -> Result<f32, SaveError> {
        match self {
            Self::Float(value) => Ok(*value),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non float property as float",
            )),
        }
    }

    pub(crate) fn read_object(&self) -> Result<ObjectReference, SaveError> {
        match self {
            Self::Object(value) => Ok(value.clone()),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non object property as object",
            )),
        }
    }

    pub(crate) fn read_linear_color(&self) -> Result<LinearColor, SaveError> {
        match self {
            Self::Struct(TypedData::LinearColor(value)) => Ok(value.clone()),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non linear color property as linear color",
            )),
        }
    }

    pub(crate) fn read_vector(&self) -> Result<Vector, SaveError> {
        match self {
            Self::Struct(TypedData::Vector(value)) => Ok(value.clone()),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non vector property as vector",
            )),
        }
    }

    pub(crate) fn read_string(&self) -> Result<String, SaveError> {
        match self {
            Self::Byte(value) => Ok(value.clone()),
            Self::Enum(value) => Ok(value.clone()),
            Self::Str(value) => Ok(value.clone()),
            _ => Err(SaveError::Parsing(
                "Trying to parse a non string property as string",
            )),
        }
    }
}

#[derive(Clone, Debug)]
pub struct EnumBaseProperty<T> {
    pub value: T,
}

#[derive(Clone, Debug)]
pub struct TextProperty {
    pub history_type: u8,
    pub namespace: Option<String>,
    pub key: Option<String>,
    pub table_id: Option<String>,
    pub text_key: Option<String>,
    pub value: String,
    pub source_fmt: Option<Box<TextProperty>>,
    pub source_text: Option<Box<TextProperty>>,
    pub transform_type: Option<u8>,
    pub arguments: Option<Vec<TextPropertyArgument>>,
}

#[derive(Clone, Debug)]
pub struct TextPropertyArgument {
    pub name: String,
    pub value_type: u8,
    pub value: Box<TextProperty>,
}
