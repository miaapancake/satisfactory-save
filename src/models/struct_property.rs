use std::collections::HashMap;

use super::{property::Property, ObjectReference, Quat, Vector, Vector2D};

#[derive(Clone, Debug)]
pub enum TypedData {
    Unknown(Box<HashMap<String, Property>>),
    Box {
        min_x: f64,
        min_y: f64,
        min_z: f64,
        max_x: f64,
        max_y: f64,
        max_z: f64,
        valid: bool,
    },
    Color {
        b: u8,
        g: u8,
        r: u8,
        a: u8,
    },
    LinearColor(LinearColor),
    FluidBox(f32),
    Quat(Quat),
    RailroadTrackPosition {
        level_name: String,
        path_name: String,
        offset: f32,
        forward: f32,
    },
    Vector(Vector),
    Vector2D(Vector2D),
    DoubleVector(Vector),
    TimerHandle(String),
    FeetOffset(FeetOffset),
    InventoryItem(InventoryItem),
    SplinePointData(SplinePointData),
    PrefabIconElement(PrefabIconElement),
    PrefabTextElement(PrefabTextElement),
    SplitterSortRule(SplitterSortRule),
    ItemCount(i32),
    Transform(Transform),
    BoomBoxPlayerState(i32),
    WasFound(bool),
    ItemAmount(ItemAmount),
    MapMarker(MapMarker),
    MessageData(MessageData),
    SpawnData(SpawnData),
    TrainSimulatedVelocity(f32),
    Empty,
}

#[derive(Debug, Clone)]
pub struct SpawnData {
    pub killed_on_day: i32,
    pub was_killed: bool,
    pub times_killed: i32,
}

#[derive(Debug, Clone)]
pub struct MessageData {
    pub message: String,
    pub was_read: bool,
}

#[derive(Debug, Clone)]
pub struct LinearColor {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

#[derive(Debug, Clone)]
pub struct MapMarker {
    pub id: u8,
    pub location: Vector,
    pub icon_id: Option<i32>,
    pub name: String,
    pub marker_type: String,
    pub scale: f32,
    pub compass_view_distance: String,
    pub color: Option<LinearColor>,
}

#[derive(Debug, Clone)]
pub struct ItemAmount {
    pub item_type: String,
    pub amount: i32,
}

#[derive(Debug, Clone)]
pub struct Transform {
    pub rotation: Option<Quat>,
    pub translation: Option<Vector>,
}

#[derive(Debug, Clone)]
pub struct SplitterSortRule {
    pub item_class: String,
    pub output_index: i32,
}

#[derive(Debug, Clone)]
pub struct PrefabIconElement {
    pub name: String,
    pub id: i32,
}

#[derive(Debug, Clone)]
pub struct PrefabTextElement {
    pub name: String,
    pub text: String,
}

#[derive(Debug, Clone)]
pub struct SplinePointData {
    pub arrive_tangent: Vector,
    pub leave_tangent: Vector,
    pub location: Vector,
}

#[derive(Debug, Clone)]
pub struct InventoryItem {
    pub item_type: String,
    pub value: ObjectReference,
    pub amount: i32,
}

#[derive(Debug, Clone)]
pub struct FeetOffset {
    pub feet_index: u8,
    pub offset_z: f32,
}
