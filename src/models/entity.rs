use std::collections::HashMap;

use super::{property::Property, ObjectReference, Quat, Vector};

#[derive(Clone, Debug)]
pub struct Entity {
    header: EntityHeader,
    pub body: EntityBody,
}

impl Entity {
    pub fn new(header: EntityHeader, body: EntityBody) -> Self {
        Self { header, body }
    }

    pub fn get_property(&self, name: &str) -> Option<&Property> {
        self.body.properties.get(name)
    }

    pub fn get_property_int(&self, name: &str) -> Option<i32> {
        if let Some(Property::Int(value)) = self.body.properties.get(name) {
            return Some(*value);
        }
        None
    }

    pub fn get_property_string(&self, name: &str) -> Option<String> {
        if let Some(Property::Str(value)) = self.body.properties.get(name) {
            return Some(value.clone());
        }
        None
    }

    pub fn get_property_object_reference(
        &self,
        name: &str,
    ) -> Option<ObjectReference> {
        if let Some(Property::Object(value)) = self.body.properties.get(name) {
            return Some(value.clone());
        }
        None
    }

    pub fn get_position(&self) -> Option<Vector> {
        match &self.header {
            EntityHeader::Actor(header) => Some(Vector {
                x: header.position_x as f64,
                y: header.position_y as f64,
                z: header.position_z as f64,
            }),
            _ => None,
        }
    }

    pub fn get_rotation(&self) -> Option<Quat> {
        match &self.header {
            EntityHeader::Actor(header) => Some(Quat {
                x: header.rotation_x as f64,
                y: header.rotation_y as f64,
                z: header.rotation_z as f64,
                w: header.rotation_w as f64,
            }),
            _ => None,
        }
    }

    pub fn get_instance_name(&self) -> &String {
        self.header.get_instance_name()
    }

    pub fn get_type(&self) -> &String {
        self.header.get_type()
    }
}

#[derive(Clone, Debug)]
pub enum EntityHeader {
    Actor(ActorHeader),
    Component(ComponentHeader),
}

impl EntityHeader {
    pub fn get_type(&self) -> &String {
        match self {
            EntityHeader::Actor(header) => &header.type_path,
            EntityHeader::Component(header) => &header.type_path,
        }
    }

    pub fn get_instance_name(&self) -> &String {
        match self {
            EntityHeader::Actor(header) => &header.instance_name,
            EntityHeader::Component(header) => &header.instance_name,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ActorHeader {
    pub type_path: String,
    pub root_object: String,
    pub instance_name: String,
    pub need_transform: i32,
    pub rotation_x: f32,
    pub rotation_y: f32,
    pub rotation_z: f32,
    pub rotation_w: f32,
    pub position_x: f32,
    pub position_y: f32,
    pub position_z: f32,
    pub scale_x: f32,
    pub scale_y: f32,
    pub scale_z: f32,
}

#[derive(Clone, Debug)]
pub struct ComponentHeader {
    pub type_path: String,
    pub root_object: String,
    pub instance_name: String,
    pub parent_actor_name: String,
}

#[derive(Default, Clone, Debug)]
pub struct EntityBody {
    pub children: Vec<ObjectReference>,
    pub properties: HashMap<String, Property>,
}
