use std::io::Seek;
use std::{collections::HashMap, io::Read};

use crate::{
    error::SaveError,
    models::{
        buildings::miner::Miner,
        entity::Entity,
        level::Level,
        resources::{
            deposit::ResourceDeposit,
            node::{get_node_info, ResourceNode},
        },
        save::Save,
    },
};

use super::reader::SaveReader;

pub struct SaveParser {
    levels: Vec<Level>,
}

impl<'a, T> From<SaveReader<'a, T>> for SaveParser
where
    T: Read + Seek + Clone,
{
    fn from(value: SaveReader<'a, T>) -> Self {
        Self {
            levels: value.sub_levels,
        }
    }
}

impl SaveParser {
    fn get_entites(&self) -> Vec<&Entity> {
        let mut entities: Vec<&Entity> = Vec::new();

        for level in self.levels.iter() {
            for entity in level.entities.iter() {
                entities.push(entity);
            }
        }

        entities
    }

    pub fn parse(&self) -> Result<Save, SaveError> {
        let mut resource_deposits: HashMap<String, ResourceDeposit> =
            HashMap::new();
        let mut resource_nodes: HashMap<String, ResourceNode> = HashMap::new();
        let mut miners: HashMap<String, Miner> = HashMap::new();

        let node_info = get_node_info()?;

        for entity in self.get_entites() {
            if ResourceDeposit::is_resource_deposit(entity) {
                resource_deposits.insert(
                    entity.get_instance_name().clone(),
                    ResourceDeposit::new(entity),
                );
            } else if ResourceNode::is_resource_node(entity) {
                let info = node_info.get(entity.get_instance_name());
                if let Some(info) = info {
                    resource_nodes.insert(
                        entity.get_instance_name().clone(),
                        ResourceNode::new(entity, info.clone()),
                    );
                }
            } else if Miner::is_miner(entity) {
                miners.insert(
                    entity.get_instance_name().clone(),
                    Miner::new(entity),
                );
            }
        }

        Ok(Save {
            resource_deposits,
            resource_nodes,
            miners,
        })
    }
}

