use std::cmp::Ordering;
use std::collections::HashMap;
use std::collections::HashSet;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;

use byteorder::ReadBytesExt;
use byteorder::LE;
use log::debug;
use log::info;
use log::warn;

use crate::error::SaveError;
use crate::models::entity::ActorHeader;
use crate::models::entity::ComponentHeader;
use crate::models::entity::Entity;
use crate::models::entity::EntityBody;
use crate::models::entity::EntityHeader;
use crate::models::level::Level;
use crate::models::property::Property;
use crate::models::property::TextProperty;
use crate::models::property::TextPropertyArgument;
use crate::models::save_header::SaveHeader;
use crate::models::struct_property::FeetOffset;
use crate::models::struct_property::InventoryItem;
use crate::models::struct_property::ItemAmount;
use crate::models::struct_property::LinearColor;
use crate::models::struct_property::MapMarker;
use crate::models::struct_property::MessageData;
use crate::models::struct_property::PrefabIconElement;
use crate::models::struct_property::PrefabTextElement;
use crate::models::struct_property::SpawnData;
use crate::models::struct_property::SplinePointData;
use crate::models::struct_property::SplitterSortRule;
use crate::models::struct_property::Transform;
use crate::models::struct_property::TypedData;
use crate::models::Element;
use crate::models::ObjectReference;
use crate::models::Quat;
use crate::models::Vector;
use crate::models::Vector2D;
use crate::parsing::read_string;

pub struct SaveReader<'a, T>
where
    T: Read + Seek + Clone,
{
    stream: &'a mut T,
    current_property_name: String,
    inside_property: bool,
    current_type: String,
    current_instance_name: String,
    unknown_data_types: HashSet<String>,
    pub header: SaveHeader,
    pub sub_levels: Vec<Level>,
}

impl<'a, T> SaveReader<'a, T>
where
    T: Read + Seek + Clone,
{
    pub fn new(stream: &'a mut T, header: SaveHeader) -> Self
    where
        T: Read + Seek + Clone,
    {
        Self {
            header,
            stream,
            unknown_data_types: HashSet::new(),
            sub_levels: Vec::new(),
            current_type: "".to_string(),
            inside_property: false,
            current_property_name: "".to_string(),
            current_instance_name: "".to_string(),
        }
    }

    pub fn read(&mut self) -> Result<(), SaveError> {
        let uncompressed_size: i64 = if self.header.save_version >= 41 {
            self.read_long()?
        } else {
            self.read_int()? as i64
        };

        info!(
            "Parsing save file of size: {} MiB",
            uncompressed_size / (1024 * 1024) // uncompressed size in MiB
        );

        if self.header.save_version >= 41 {
            let unknown_count = self.read_int()?; // unknown
            self.read_string()?; // unknown
            self.read_long()?; // unknown
            self.read_int()?; // unknown
            self.read_string()?; // unknown
            self.read_int()?; // unknown
            debug!("Reading {unknown_count} unknown items!");
            for _ in 1..unknown_count {
                let name = self.read_string()?;
                debug!("UNKNOWN DATA: {name}:");
                self.read_int()?; // ???
                self.read_int()?; // ???
                                  //
                let unknown_count2 = self.read_int()?;

                debug!("Reading {unknown_count2} unknown items inside {name}!");

                for _ in 0..unknown_count2 {
                    self.read_string()?; // unknown
                    self.read_uint()?; // unknown
                }
            }
        }

        let sublevel_count = self.read_int()?;

        info!(
            "Reading {sublevel_count} levels at {}",
            self.get_position()?
        );

        let mut sublevels: Vec<Level> = Vec::new();

        for _ in 0..sublevel_count {
            sublevels.push(self.read_level(false)?);
        }

        sublevels.push(self.read_level(true)?);

        self.sub_levels = sublevels;

        println!("Unknown types list: {:#?}", self.unknown_data_types);

        Ok(())
    }

    fn read_level(
        &mut self,
        persistent_level: bool,
    ) -> Result<Level, SaveError> {
        let mut sublevel_name: String = "PersistentLevel".to_string();

        if !persistent_level {
            sublevel_name = self.read_string()?;
        }

        debug!(
            "Reading Level: {sublevel_name} at {:X}",
            self.get_position()?
        );

        let level_size: i64 = if self.header.save_version >= 41 {
            self.read_long()?
        } else {
            self.read_int()? as i64
        };

        debug!("Level size: {level_size} bytes");

        let start_position = self.get_position()?;

        let end_of_level_position = start_position + level_size as u64;

        let header_count = self.read_int()?;

        let mut headers: Vec<EntityHeader> =
            Vec::with_capacity(header_count as usize);

        for _ in 0..header_count {
            headers.push(self.read_entity_header()?);
        }

        let mut collectables: Vec<ObjectReference> = Vec::new();

        if self.header.save_version >= 41 {
            match self.get_position()?.cmp(&(end_of_level_position - 4)) {
                Ordering::Less => {
                    let collectables_count = self.read_int()?;
                    for _ in 0..collectables_count {
                        collectables.push(self.read_object_reference()?);
                    }
                }
                Ordering::Equal => {
                    warn!("Skipping collectables");
                    self.read_int()?;
                }
                _ => (),
            };
        } else {
            let collectables_count = self.read_int()?;
            for _ in 0..collectables_count {
                collectables.push(self.read_object_reference()?);
            }
        }

        if self.header.save_version >= 41 {
            self.seek_by(8)?; // Objects size
        } else {
            self.seek_by(4)?; // Objects size
        }

        self.seek_by(4)?; // Objects count

        let mut entities: Vec<Entity> =
            Vec::with_capacity(header_count as usize);

        for header in headers {
            entities.push(self.read_entity(header)?);
        }

        for _ in 0..self.read_int()? {
            self.read_object_reference()?;
        }

        Ok(Level {
            sublevel_name,
            collectables,
            entities,
        })
    }

    fn read_entity_header(&mut self) -> Result<EntityHeader, SaveError> {
        let header_type = self.read_int()?;

        match header_type {
            0 => Ok(EntityHeader::Component(self.read_component_header()?)),
            1 => Ok(EntityHeader::Actor(self.read_actor_header()?)),
            _ => panic!("Invalid header type: {header_type}"),
        }
    }

    fn read_component_header(&mut self) -> Result<ComponentHeader, SaveError> {
        Ok(ComponentHeader {
            type_path: self.read_string()?,
            root_object: self.read_string()?,
            instance_name: self.read_string()?,
            parent_actor_name: self.read_string()?,
        })
    }

    fn read_actor_header(&mut self) -> Result<ActorHeader, SaveError> {
        let header = ActorHeader {
            type_path: self.read_string()?,
            root_object: self.read_string()?,
            instance_name: self.read_string()?,
            need_transform: self.read_int()?,
            rotation_x: self.read_float()?,
            rotation_y: self.read_float()?,
            rotation_z: self.read_float()?,
            rotation_w: self.read_float()?,
            position_x: self.read_float()?,
            position_y: self.read_float()?,
            position_z: self.read_float()?,
            scale_x: self.read_float()?,
            scale_y: self.read_float()?,
            scale_z: self.read_float()?,
        };

        // Skip unclear byte
        self.seek_by(4)?;
        Ok(header)
    }

    fn read_entity(
        &mut self,
        header: EntityHeader,
    ) -> Result<Entity, SaveError> {
        self.current_type = header.get_type().clone();
        self.current_instance_name = header.get_instance_name().clone();

        if self.header.save_version >= 41 {
            self.read_int()?;
            self.read_int()?;
        }

        let size = self.read_int()?;
        let start_pos = self.get_position()?;
        let end_pos = start_pos + size as u64;

        let mut children: Vec<ObjectReference> = Vec::new();

        if let EntityHeader::Actor(_) = header.clone() {
            self.read_object_reference()?;

            let component_count = self.read_int()?;

            for _ in 0..component_count {
                children.push(self.read_object_reference()?);
            }
        }

        let mut properties: HashMap<String, Property> = HashMap::new();

        if self.get_position()? < end_pos {
            properties = self.read_properties()?;
        }

        if self.get_position()? < end_pos {
            let current_pos = self.get_position()?;
            let delta = (end_pos - current_pos) as i64;
            warn!("Skipping {delta} trailing bytes");
            self.seek_by(delta)?;
        }

        Ok(Entity::new(
            header,
            EntityBody {
                children,
                properties,
            },
        ))
    }

    fn read_property(
        &mut self,
    ) -> Result<Option<(String, Property)>, SaveError> {
        let start_pos = self.get_position()?;

        let property_name = self.read_string()?;

        if property_name == "None" {
            debug!("Stopping at NONE property at {:X}", start_pos);
            return Ok(None);
        }

        if !self.inside_property {
            self.current_property_name = property_name.clone();
            self.inside_property = true;
        }

        if self.read_byte()? != 0 {
            self.seek_by(-1)?;
        } else {
            warn!("Found extra byte!");
        }

        let property_type = self.read_string()?;
        self.read_int()?; // size

        self.read_int()?; // index;

        debug!(
            "Reading property {property_name} of type {property_type} at {:X}",
            start_pos
        );

        let property = Some((
            property_name.clone(),
            match property_type.as_str() {
                "BoolProperty" => {
                    let value = self.read_byte()? != 0;
                    self.read_property_uuid()?; // skip UUID
                    Property::Bool(value)
                }
                "UInt32Property" => {
                    self.read_property_uuid()?;
                    Property::UInt(self.read_uint()?)
                }
                "IntProperty" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Int(self.read_int()?)
                }
                "Int8Property" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Int8(self.read_int8()?)
                }
                "Int64Property" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Int64(self.read_long()?)
                }
                "DoubleProperty" => {
                    self.read_property_uuid()?;
                    Property::Double(self.read_double()?)
                }
                "FloatProperty" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Float(self.read_float()?)
                }
                "StrProperty" | "NameProperty" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Str(self.read_string()?)
                }
                "EnumProperty" => {
                    self.read_string()?; // enum Name
                    self.read_property_uuid()?; // skip UUID
                    Property::Enum(self.read_string()?)
                }
                "ByteProperty" => {
                    let byte_name = self.read_string()?;
                    self.read_property_uuid()?; // skip UUID
                    if byte_name == "None" {
                        Property::NoneByte(self.read_byte()?)
                    } else {
                        Property::Byte(self.read_string()?)
                    }
                }
                "StructProperty" => {
                    Property::Struct(self.read_struct_property()?)
                }
                "ObjectProperty" | "InterfaceProperty" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Object(self.read_object_reference()?)
                }
                "TextProperty" => {
                    self.read_property_uuid()?; // skip UUID
                    Property::Text(self.read_text_property()?)
                }
                "ArrayProperty" => Property::Array(
                    self.read_array_property(property_name.clone())?,
                ),
                "MapProperty" => Property::Map(self.read_map_property()?),
                "SetProperty" => Property::Set(self.read_set_property()?),
                _ => panic!(
                    "Unknown property type: {property_type} at offset: {:X}",
                    self.get_position()?
                ),
            },
        ));

        if property_name == self.current_property_name {
            self.inside_property = false;
        }

        Ok(property)
    }

    fn read_array_property(
        &mut self,
        name: String,
    ) -> Result<Vec<Element>, SaveError> {
        let element_type = self.read_string()?;

        self.seek_by(1)?;

        let element_count = self.read_int()?;

        let mut array_property = Vec::new();

        debug!(
            "Reading Array Property of type: {} of length {}",
            element_type, element_count
        );

        match element_type.as_str() {
            "ByteProperty" => {
                if name == "mFogOfWarRawData" {
                    for _ in 0..element_count / 4 {
                        self.read_byte()?; //0
                        self.read_byte()?; //0
                        array_property.push(Element::Byte(self.read_byte()?));
                        self.read_byte()?; //255
                    }
                } else {
                    for _ in 0..element_count {
                        array_property.push(Element::Byte(self.read_byte()?))
                    }
                }
            }
            "BoolProperty" => {
                for _ in 0..element_count {
                    array_property.push(Element::Bool(self.read_byte()? != 0))
                }
            }
            "IntProperty" => {
                for _ in 0..element_count {
                    array_property.push(Element::Int(self.read_int()?))
                }
            }
            "Int64Property" => {
                for _ in 0..element_count {
                    array_property.push(Element::Int64(self.read_long()?))
                }
            }
            "FloatProperty" => {
                for _ in 0..element_count {
                    array_property.push(Element::Float(self.read_float()?))
                }
            }
            "EnumProperty" => {
                for _ in 0..element_count {
                    array_property.push(Element::Enum(self.read_string()?))
                }
            }
            "ObjectProperty" => {
                for _ in 0..element_count {
                    array_property
                        .push(Element::Object(self.read_object_reference()?))
                }
            }
            "TextProperty" => {
                for _ in 0..element_count {
                    array_property
                        .push(Element::Text(self.read_text_property()?))
                }
            }
            "InterfaceProperty" => {
                for _ in 0..element_count {
                    array_property
                        .push(Element::Interface(self.read_object_reference()?))
                }
            }
            "StrProperty" => {
                for _ in 0..element_count {
                    array_property.push(Element::Str(self.read_string()?))
                }
            }
            "StructProperty" => {
                self.read_string()?;
                let struct_type = self.read_string()?; // StructProperty
                assert_eq!(struct_type, "StructProperty");

                self.read_int()?; // structureSize
                self.read_int()?; // 0

                let sub_type = self.read_string()?;

                self.seek_by(17)?;

                let mut value: Vec<TypedData> = Vec::new();

                for _ in 0..element_count {
                    match sub_type.as_str() {
                        "InventoryItem" => {
                            value.push(TypedData::InventoryItem(
                                InventoryItem {
                                    amount: self.read_int()?,
                                    item_type: self.read_string()?,
                                    value: self.read_object_reference()?,
                                },
                            ));
                        }
                        _ => {
                            value.push(self.read_typed_data(sub_type.clone())?)
                        }
                    };
                }

                array_property.push(Element::ArrayStruct(value));
            }
            _ => panic!(
                "Unknown array property type {element_type} at {:X}!",
                self.get_position()?
            ),
        }

        Ok(array_property)
    }
    fn read_map_property(
        &mut self,
    ) -> Result<HashMap<String, Element>, SaveError> {
        let start_pos = self.get_position()?;
        let key_type = self.read_string()?; // key type
        let value_type = self.read_string()?;
        let mut value: HashMap<String, Element> = HashMap::new();

        self.seek_by(1)?;

        let mode_type = self.read_int()?;

        if mode_type == 2 {
            self.read_string()?;
            self.read_string()?;
        } else if mode_type == 3 {
            self.seek_by(9)?;
            self.read_string()?;
            self.read_string()?;
        }

        let key_value_count = self.read_int()?;

        debug!(
            "Reading map({}) of type {}:{} with {key_value_count} items at {start_pos:X}",
            self.current_property_name, key_type, value_type
        );

        for i in 0..key_value_count {
            debug!(
                "Reading the {i}th item in map({key_value_count}) at {:X}",
                self.get_position()?
            );

            debug!(
                "Reading the {i}th item's key in map at {:X}",
                self.get_position()?
            );
            let key: String = match key_type.as_str() {
                "IntProperty" => format!("{}", self.read_int()?),
                "Int64Property" => format!("{}", self.read_long()?),
                "NameProperty" | "StrProperty" | "EnumProperty" => {
                    self.read_string()?
                },
                "StructProperty" if self.current_type == "/BuildGunUtilities/BGU_Subsystem.BGU_Subsystem_C" => {
                        format!(
                            "{},{},{}",
                            self.read_float()?,
                            self.read_float()?,
                            self.read_float()?
                        )
                },
                "StructProperty" if self.current_property_name == "mSaveData" => {
                        warn!("mSaveData");
                        format!(
                            "{},{},{}",
                            self.read_int()?,
                            self.read_int()?,
                            self.read_int()?
                        )

                },
                "StructProperty" if self.current_property_name == "mUnresolvedSaveData" => {
                        warn!("mSaveData");
                        format!(
                            "{},{},{}",
                            self.read_int()?,
                            self.read_int()?,
                            self.read_int()?
                        )

                },
                "StructProperty" if self.current_property_name == "Destroyed_Foliage_Transform" => {

                    warn!("FoliageData");

                    if self.header.save_version >= 41 {
                        format!(
                            "{},{},{}",
                            self.read_double()?,
                            self.read_double()?,
                            self.read_double()?
                        )
                    } else {
                        format!(
                            "{},{},{}",
                            self.read_float()?,
                            self.read_float()?,
                            self.read_float()?
                        )
                    }
                },
                "StructProperty" => {
                    self.read_properties()?;
                    "".to_string()
                }
                "ObjectProperty" => {
                    format!("{}_{}", self.read_string()?, self.read_string()?)
                },
                _ => panic!("Unknown map key type {key_type}"),
            };

            debug!(
                "Reading the {i}th item's value in map at {:X}",
                self.get_position()?
            );
            match value_type.as_str() {
                "ByteProperty" => {
                    value.insert(key.clone(), Element::Byte(self.read_byte()?));
                }
                "BoolProperty" => {
                    value.insert(
                        key.clone(),
                        Element::Bool(self.read_byte()? != 0),
                    );
                }
                "IntProperty" => {
                    value.insert(key.clone(), Element::Int(self.read_int()?));
                }
                "FloatProperty" => {
                    value.insert(
                        key.clone(),
                        Element::Float(self.read_float()?),
                    );
                }
                "StrProperty" => {
                    value
                        .insert(key.clone(), Element::Str(self.read_string()?));
                }
                "ObjectProperty" => {
                    value.insert(
                        key.clone(),
                        Element::Object(self.read_object_reference()?),
                    );
                }
                "StructProperty" => {
                    let mut struct_value: HashMap<String, Property> =
                        HashMap::new();

                    match self.current_type.as_str() {
                        "LBBalancerData" => {
                            self.read_int()?;
                            self.read_int()?;
                            self.read_int()?;
                        }

                        "/StorageStatsRoom/Sub_SR.Sub_SR_C"
                        | "/CentralStorage/Subsystem_SC.Subsystem_SC_C" => {
                            if self.header.save_version >= 41 {
                                self.read_double()?;
                                self.read_double()?;
                                self.read_double()?;
                            } else {
                                self.read_float()?;
                                self.read_float()?;
                                self.read_float()?;
                            }
                        }
                        _ => {
                            struct_value = self.read_properties()?;
                        }
                    };

                    value.insert(
                        key.clone(),
                        Element::Unknown(Box::new(struct_value)),
                    );
                }
                _ => panic!("Unknown map value type: {value_type}"),
            }
        }

        debug!("End of map at {:X}", self.get_position()?);

        Ok(value)
    }

    fn read_set_property(&mut self) -> Result<Vec<Element>, SaveError> {
        let start_pos = self.get_position()?;
        let set_type = self.read_string()?;

        self.seek_by(5)?;

        let set_items_count = self.read_int()?;

        debug!(
            "Reading set of type: {} of length {} at {:X}",
            set_type, set_items_count, start_pos
        );

        let mut value: Vec<Element> = Vec::new();

        for _ in 0..set_items_count {
            match set_type.as_str() {
                "ObjectProperty" => {
                    value.push(Element::Object(self.read_object_reference()?))
                }
                "IntProperty" => value.push(Element::Int(self.read_int()?)),
                "UInt32Property" => {
                    value.push(Element::UInt(self.read_uint()?))
                }
                "StructProperty" => {
                    if self.current_type
                        == "/Script/FactoryGame.FGFoliageRemoval"
                    {
                        value.push(Element::Vector(Vector {
                            x: self.read_float()? as f64,
                            y: self.read_float()? as f64,
                            z: self.read_float()? as f64,
                        }))
                    } else {
                        warn!("UNKNOWNF SET PROPERTY: {set_type}");
                    }
                }
                _ => panic!("Unknown set property type: {set_type}"),
            }
        }

        Ok(value)
    }

    fn read_text_property(&mut self) -> Result<TextProperty, SaveError> {
        self.read_int()?; // flags
        let history_type = self.read_byte()?; //history type

        let mut value = TextProperty {
            value: "".to_string(),
            history_type,
            key: None,
            namespace: None,
            source_fmt: None,
            arguments: None,
            source_text: None,
            transform_type: None,
            table_id: None,
            text_key: None,
        };

        match history_type {
            0 => {
                value.namespace = Some(self.read_string()?);
                value.key = Some(self.read_string()?);
                value.value = self.read_string()?;
            }
            1 | 3 => {
                value.source_fmt = Some(Box::new(self.read_text_property()?));

                let argument_count = self.read_int()?;

                let mut arguments: Vec<TextPropertyArgument> =
                    Vec::with_capacity(argument_count as usize);

                for _ in 0..argument_count {
                    arguments.push(TextPropertyArgument {
                        name: self.read_string()?,
                        value_type: self.read_byte()?,
                        value: Box::new(self.read_text_property()?),
                    });
                }
                value.arguments = Some(arguments);
            }
            10 => {
                value.source_text = Some(Box::new(self.read_text_property()?));
                value.transform_type = Some(self.read_byte()?);
            }
            11 => {
                value.table_id = Some(self.read_string()?);
                value.text_key = Some(self.read_string()?);
            }
            255 => {
                if self.read_int()? == 1 {
                    value.value = self.read_string()?;
                }
            }
            _ => panic!("Unknown history_type {history_type}"),
        }

        Ok(value)
    }

    fn read_struct_property(&mut self) -> Result<TypedData, SaveError> {
        let struct_type = self.read_string()?; // Struct Type
        self.seek_by(17)?;

        debug!("Reading typed data in structproperty of type: {struct_type}");

        self.read_typed_data(struct_type)
    }

    fn read_properties(
        &mut self,
    ) -> Result<HashMap<String, Property>, SaveError> {
        let mut properties: HashMap<String, Property> = HashMap::new();
        let start_pos = self.get_position()?;

        debug!("READING PROPERTY ARRAY ON {:X}", start_pos);

        loop {
            match self.read_property()? {
                None => break,
                Some((name, value)) => {
                    properties.insert(name, value);
                }
            }
        }

        Ok(properties)
    }

    fn read_message_data(&mut self) -> Result<MessageData, SaveError> {
        let properties = self.read_properties()?;

        let message_class = properties
            .get("MessageClass")
            .ok_or(SaveError::Parsing("No MessageClass in Message Data"))?;

        let was_read =
            properties.get("WasRead").unwrap_or(&Property::Bool(false));

        Ok(MessageData {
            message: message_class.read_object()?.path_name,
            was_read: was_read.read_bool()?,
        })
    }

    fn read_text_prefab(&mut self) -> Result<PrefabTextElement, SaveError> {
        let properties = self.read_properties()?;

        let name = properties
            .get("ElementName")
            .ok_or(SaveError::Parsing("Element Name missing in text prefab"))?;

        let text = properties
            .get("Text")
            .ok_or(SaveError::Parsing("Text missing in text prefab"))?;

        Ok(PrefabTextElement {
            name: name.read_string()?,
            text: text.read_string()?,
        })
    }

    fn read_icon_prefab(&mut self) -> Result<PrefabIconElement, SaveError> {
        let properties = self.read_properties()?;

        let name = properties
            .get("ElementName")
            .ok_or(SaveError::Parsing("Element Name missing in icon prefab"))?;

        let id = properties
            .get("IconID")
            .ok_or(SaveError::Parsing("Icon ID missing in icon prefab"))?;

        Ok(PrefabIconElement {
            name: name.read_string()?,
            id: id.read_int()?,
        })
    }

    fn read_inventory_stack(&mut self) -> Result<TypedData, SaveError> {
        let properties = self.read_properties()?;

        let item = properties.get("Item");

        match item {
            Some(Property::Struct(TypedData::InventoryItem(item))) => {
                Ok(TypedData::InventoryItem(item.clone()))
            }
            Some(_) => {
                warn!(
                    "Inventory Stack witht unexpected Item Type. Data:\n{:#?}",
                    properties
                );
                Ok(TypedData::Empty)
            }
            None => {
                let item_amount = properties.get("NumItems");
                match item_amount {
                    Some(Property::Int(amount)) => {
                        Ok(TypedData::ItemCount(*amount))
                    }
                    _ => {
                        warn!(
                            "Invalid InventoryStack Data:\n{:#?}",
                            properties
                        );
                        Ok(TypedData::Empty)
                    }
                }
            }
        }
    }

    fn read_spline_point_data(&mut self) -> Result<SplinePointData, SaveError> {
        let properties = self.read_properties()?;

        let arrive_tangent = properties
            .get("ArriveTangent")
            .ok_or(SaveError::Parsing("No arrive tangent found"))?;

        let leave_tangent = properties
            .get("LeaveTangent")
            .ok_or(SaveError::Parsing("No leave tangent found"))?;

        let location = properties
            .get("Location")
            .ok_or(SaveError::Parsing("No location tangent found"))?;

        Ok(SplinePointData {
            arrive_tangent: arrive_tangent.read_vector()?,
            leave_tangent: leave_tangent.read_vector()?,
            location: location.read_vector()?,
        })
    }

    fn read_transform(&mut self) -> Result<Transform, SaveError> {
        let properties = self.read_properties()?;

        let rotation_prop = properties.get("Rotation");
        let translation_prop = properties.get("Translation");

        match (translation_prop, rotation_prop) {
            (
                Some(Property::Struct(TypedData::Vector(translation))),
                Some(Property::Struct(TypedData::Quat(rotation))),
            ) => Ok(Transform {
                translation: Some(translation.clone()),
                rotation: Some(rotation.clone()),
            }),
            (Some(Property::Struct(TypedData::Vector(translation))), None) => {
                Ok(Transform {
                    rotation: None,
                    translation: Some(translation.clone()),
                })
            }
            (None, Some(Property::Struct(TypedData::Quat(rotation)))) => {
                Ok(Transform {
                    rotation: Some(rotation.clone()),
                    translation: None,
                })
            }
            _ => Ok(Transform {
                rotation: None,
                translation: None,
            }),
        }
    }

    fn read_splitter_sort_rule(
        &mut self,
    ) -> Result<SplitterSortRule, SaveError> {
        let properties = self.read_properties()?;

        let item_class = properties.get("ItemClass").ok_or(
            SaveError::Parsing("Missing ItemClass in SplitterSortRule"),
        )?;

        let output_index = properties.get("OutputIndex").ok_or(
            SaveError::Parsing("Missing OutputIndex in SplitterSortRule"),
        )?;

        let Property::Object(item_class) = item_class else {
            Err(SaveError::Parsing("Wrong type for ItemClass in SplitterSortRule"))?
        };

        let Property::Int(output_index) = output_index else {
            Err(SaveError::Parsing("Wrong type for OutputIndex in SplitterSortRule"))?
        };

        let item_class = item_class.path_name.clone();

        Ok(SplitterSortRule {
            item_class,
            output_index: *output_index,
        })
    }

    fn read_feet_offset(&mut self) -> Result<FeetOffset, SaveError> {
        let properties = self.read_properties()?;

        let feet_index = properties.get("FeetIndex");
        let offset_z = properties.get("OffsetZ");

        let Some(Property::NoneByte(feet_index)) = feet_index else {
            Err(SaveError::Parsing("Wrong type for or no FeetIndex in FeetOffset"))?
        };

        let Some(Property::Float(offset_z)) = offset_z else {
            Err(SaveError::Parsing("Wrong type for or no OffzetZ in FeetOffset"))?
        };

        Ok(FeetOffset {
            feet_index: *feet_index,
            offset_z: *offset_z,
        })
    }

    fn read_typed_data(
        &mut self,
        struct_type: String,
    ) -> Result<TypedData, SaveError> {
        Ok(match struct_type.as_str() {
            "Guid" => {
                self.seek_by(16)?;
                TypedData::Empty
            }
            "Color" => TypedData::Color {
                b: self.read_byte()?,
                g: self.read_byte()?,
                r: self.read_byte()?,
                a: self.read_byte()?,
            },
            "LinearColor" => TypedData::LinearColor(LinearColor {
                r: self.read_float()?,
                g: self.read_float()?,
                b: self.read_float()?,
                a: self.read_float()?,
            }),
            "Vector2D" if self.header.save_version >= 41 => {
                TypedData::Vector2D(Vector2D {
                    x: self.read_double()?,
                    y: self.read_double()?,
                })
            }
            "Vector2D" if self.header.save_version < 41 => {
                TypedData::Vector2D(Vector2D {
                    x: self.read_float()? as f64,
                    y: self.read_float()? as f64,
                })
            }
            "Vector" | "Rotator" if self.header.save_version >= 41 => {
                TypedData::Vector(Vector {
                    x: self.read_double()?,
                    y: self.read_double()?,
                    z: self.read_double()?,
                })
            }
            "Vector" | "Rotator" if self.header.save_version < 41 => {
                TypedData::Vector(Vector {
                    x: self.read_float()? as f64,
                    y: self.read_float()? as f64,
                    z: self.read_float()? as f64,
                })
            }
            "Quat" | "Vector4" if self.header.save_version < 41 => {
                TypedData::Quat(Quat {
                    x: self.read_float()? as f64,
                    y: self.read_float()? as f64,
                    z: self.read_float()? as f64,
                    w: self.read_float()? as f64,
                })
            }
            "Quat" | "Vector4" if self.header.save_version >= 41 => {
                TypedData::Quat(Quat {
                    x: self.read_double()?,
                    y: self.read_double()?,
                    z: self.read_double()?,
                    w: self.read_double()?,
                })
            }
            "Box" if self.header.save_version < 41 => TypedData::Box {
                min_x: self.read_float()? as f64,
                min_y: self.read_float()? as f64,
                min_z: self.read_float()? as f64,
                max_x: self.read_float()? as f64,
                max_y: self.read_float()? as f64,
                max_z: self.read_float()? as f64,
                valid: self.read_byte()? != 0,
            },
            "Box" if self.header.save_version >= 41 => TypedData::Box {
                min_x: self.read_double()?,
                min_y: self.read_double()?,
                min_z: self.read_double()?,
                max_x: self.read_double()?,
                max_y: self.read_double()?,
                max_z: self.read_double()?,
                valid: self.read_byte()? != 0,
            },

            "TimerHandle" => TypedData::TimerHandle(self.read_string()?),
            "FluidBox" => TypedData::FluidBox(self.read_float()?),
            "InventoryItem" => {
                TypedData::InventoryItem(self.read_inventory_item()?)
            }
            "InventoryStack" => self.read_inventory_stack()?,
            "RailroadTrackPosition" => TypedData::RailroadTrackPosition {
                level_name: self.read_string()?,
                path_name: self.read_string()?,
                offset: self.read_float()?,
                forward: self.read_float()?,
            },
            "FeetOffset" => TypedData::FeetOffset(self.read_feet_offset()?),
            "SplinePointData" => {
                TypedData::SplinePointData(self.read_spline_point_data()?)
            }
            "PrefabIconElementSaveData" => {
                TypedData::PrefabIconElement(self.read_icon_prefab()?)
            }
            "PrefabTextElementSaveData" => {
                TypedData::PrefabTextElement(self.read_text_prefab()?)
            }
            "SplitterSortRule" => {
                TypedData::SplitterSortRule(self.read_splitter_sort_rule()?)
            }
            "BoomBoxPlayerState" => {
                TypedData::BoomBoxPlayerState(self.read_boombox_player_state()?)
            }
            "ItemFoundData" => TypedData::WasFound(self.read_was_found()?),
            "ItemAmount" => TypedData::ItemAmount(self.read_item_amount()?),
            "Vector_NetQuantize" => {
                TypedData::Vector(self.read_vector_net_quantize()?)
            }
            "MessageData" => TypedData::MessageData(self.read_message_data()?),
            "MapMarker" => TypedData::MapMarker(self.read_map_marker()?),
            "SpawnData" => TypedData::SpawnData(self.read_spawn_data()?),
            "TrainSimulationData" => TypedData::TrainSimulatedVelocity(
                self.read_train_simulated_velocity()?,
            ),
            "Transform" => TypedData::Transform(self.read_transform()?),
            _ => {
                let properties = self.read_properties()?;
                warn!("Unknown typeddata type (TRYING): {struct_type}");
                warn!("Data: {:#?}", properties);
                self.unknown_data_types.insert(struct_type);
                TypedData::Unknown(Box::new(properties))
            }
        })
    }

    fn read_train_simulated_velocity(&mut self) -> Result<f32, SaveError> {
        self.read_properties()?
            .get("Velocity")
            .unwrap_or(&Property::Float(0.0))
            .read_float()
    }

    fn read_spawn_data(&mut self) -> Result<SpawnData, SaveError> {
        let properties = self.read_properties()?;

        let killed_on_day = properties
            .get("KilledOnDayNr")
            .ok_or(SaveError::Parsing("No killed on day on spawndata"))?
            .read_int()?;

        let was_killed = properties
            .get("WasKilled")
            .ok_or(SaveError::Parsing("No was killed on spawndata"))?
            .read_bool()?;

        let times_killed = properties
            .get("NumTimesKilled")
            .ok_or(SaveError::Parsing("No times killed on spawndata"))?
            .read_int()?;

        Ok(SpawnData {
            killed_on_day,
            was_killed,
            times_killed,
        })
    }

    fn read_property_uuid(&mut self) -> Result<(), SaveError> {
        let has_guuid = self.read_byte()?;

        if has_guuid == 1 {
            self.read_int()?;
            self.read_int()?;
            self.read_int()?;
            self.read_int()?;
        }

        Ok(())
    }

    fn read_map_marker(&mut self) -> Result<MapMarker, SaveError> {
        let properties = self.read_properties()?;

        let id = properties
            .get("MarkerID")
            .ok_or(SaveError::Parsing("No ID in map marker"))?
            .read_none_byte()?;

        let location = properties
            .get("Location")
            .ok_or(SaveError::Parsing("No location in map marker"))?
            .read_vector()?;

        let name = properties
            .get("Name")
            .unwrap_or(&Property::Str("".to_string()))
            .read_string()?;

        let marker_type = properties
            .get("MapMarkerType")
            .ok_or(SaveError::Parsing("No marker type in map marker"))?
            .read_string()?;

        let scale = properties
            .get("Scale")
            .unwrap_or(&Property::Float(1.0))
            .read_float()?;

        let compass_view_distance = properties
            .get("CompassViewDistance")
            .unwrap_or(&Property::Str("".to_string()))
            .read_string()?;

        let icon_id = properties.get("IconID").map(|p| p.read_int().unwrap());

        let color = properties
            .get("Color")
            .map(|p| p.read_linear_color().unwrap());

        Ok(MapMarker {
            id,
            location,
            name,
            marker_type,
            scale,
            compass_view_distance,
            icon_id,
            color,
        })
    }

    fn read_vector_net_quantize(&mut self) -> Result<Vector, SaveError> {
        let properties = self.read_properties()?;

        let x = properties
            .get("X")
            .ok_or(SaveError::Parsing("No X in vector net quantize"))?;

        let y = properties
            .get("Y")
            .ok_or(SaveError::Parsing("No Y in vector net quantize"))?;

        let z = properties
            .get("Z")
            .ok_or(SaveError::Parsing("No Z in vector net quantize"))?;

        if self.header.save_version >= 41 {
            Ok(Vector {
                x: x.read_double()?,
                y: y.read_double()?,
                z: z.read_double()?,
            })
        } else {
            Ok(Vector {
                x: x.read_float()? as f64,
                y: y.read_float()? as f64,
                z: z.read_float()? as f64,
            })
        }
    }

    fn read_item_amount(&mut self) -> Result<ItemAmount, SaveError> {
        let properties = self.read_properties()?;

        let item_class = properties
            .get("ItemClass")
            .ok_or(SaveError::Parsing("No ItemClass in ItemAmount"))?;

        let amount = properties
            .get("Amount")
            .ok_or(SaveError::Parsing("No Amount in ItemAmount"))?;

        match (item_class, amount) {
            (Property::Object(item_class), Property::Int(amount)) => {
                Ok(ItemAmount {
                    item_type: item_class.path_name.clone(),
                    amount: *amount,
                })
            }
            _ => Err(SaveError::Parsing("Invalid ItemAmount")),
        }
    }

    fn read_boombox_player_state(&mut self) -> Result<i32, SaveError> {
        self.read_properties()?
            .get("mPlaybackState")
            .ok_or(SaveError::Parsing(
                "No mPlayerbackState on boombox player state",
            ))?
            .read_int()
    }

    fn read_was_found(&mut self) -> Result<bool, SaveError> {
        self.read_properties()?
            .get("WasFound")
            .ok_or(SaveError::Parsing("No WasFound on ItemFoundData"))?
            .read_bool()
    }

    fn read_inventory_item(&mut self) -> Result<InventoryItem, SaveError> {
        self.read_int()?;
        let item_type = self.read_string()?;
        let value = self.read_object_reference()?;

        let amount = self
            .read_property()?
            .ok_or(SaveError::Parsing("No amount in inventory item"))?;

        Ok(InventoryItem {
            item_type,
            value,
            amount: amount.1.read_int()?,
        })
    }

    fn read_object_reference(&mut self) -> Result<ObjectReference, SaveError> {
        Ok(ObjectReference {
            level_name: self.read_string()?,
            path_name: self.read_string()?,
        })
    }

    fn get_position(&mut self) -> Result<u64, SaveError> {
        Ok(self.stream.stream_position()?)
    }

    fn seek_by(&mut self, amount: i64) -> Result<(), SaveError> {
        self.stream.seek(SeekFrom::Current(amount))?;
        Ok(())
    }

    fn read_string(&mut self) -> Result<String, SaveError> {
        Ok(read_string(&mut self.stream)?)
    }

    fn read_int(&mut self) -> Result<i32, SaveError> {
        Ok(self.stream.read_i32::<LE>()?)
    }

    fn read_uint(&mut self) -> Result<u32, SaveError> {
        Ok(self.stream.read_u32::<LE>()?)
    }

    fn read_int8(&mut self) -> Result<i8, SaveError> {
        Ok(self.stream.read_i8()?)
    }

    fn read_long(&mut self) -> Result<i64, SaveError> {
        Ok(self.stream.read_i64::<LE>()?)
    }

    fn read_float(&mut self) -> Result<f32, SaveError> {
        Ok(self.stream.read_f32::<LE>()?)
    }

    fn read_double(&mut self) -> Result<f64, SaveError> {
        Ok(self.stream.read_f64::<LE>()?)
    }

    fn read_byte(&mut self) -> Result<u8, SaveError> {
        Ok(self.stream.read_u8()?)
    }
}
