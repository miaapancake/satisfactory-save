use byteorder::{LittleEndian, ReadBytesExt};
use log::error;
use std::borrow::BorrowMut;
use std::io::Read;
use std::io::{self, Seek};
use widestring::Utf16String;

pub mod compression;
pub mod parser;
pub mod reader;

pub fn read_bytes<T>(stream: &mut T, length: u64) -> io::Result<Vec<u8>>
where
    T: Read + ReadBytesExt + Seek,
{
    let mut buffer = Vec::with_capacity(length as usize);
    stream.take(length).read_to_end(&mut buffer)?;
    Ok(buffer)
}

pub fn read_long<T>(stream: T) -> io::Result<i64>
where
    T: Read + ReadBytesExt + Seek,
{
    stream.take(8).read_i64::<LittleEndian>()
}

pub fn read_string<T>(stream: &mut T) -> io::Result<String>
where
    T: Read + ReadBytesExt + Seek,
{
    let last_pos = stream.stream_position()?;
    let mut len: i32 = stream.borrow_mut().read_i32::<LittleEndian>()?;

    if len == 0 {
        return Ok("".to_string());
    }

    if len < 0 {
        len = -len;
        let mut str_buffer: Vec<u16> = Vec::with_capacity(len as usize);
        for _ in 0..len {
            str_buffer.push(stream.read_u16::<LittleEndian>()?);
        }

        let str = Utf16String::from_vec(str_buffer)
            .expect("Failed to convert buffer to UTF-16 string");

        return Ok(str.into());
    }

    if len > 64000 {
        panic!(
            "Trying to read string of length {len} at offset: {:X}!",
            stream.stream_position()? - 4
        );
    }

    let mut str_buffer = String::new();
    let result = stream.take(len as u64).read_to_string(&mut str_buffer);

    if let Err(err) = result {
        error!("Error while trying to read string at {last_pos:X}",);
        panic!("Could not read string: {err}");
    }

    Ok(str_buffer.replace('\0', ""))
}
