use std::{
    io::{Cursor, Read, Seek, SeekFrom},
    thread::available_parallelism,
    thread::{self, JoinHandle},
};

use byteorder::{LittleEndian, ReadBytesExt};
use compress::zlib;
use log::{debug, info};

use crate::{
    error::SaveError, models::save_header::SaveHeader, parsing::read_bytes,
};

const PACKET_SIGNATURE: u32 = 0x9E2A83C1;

fn decompress_chunk(
    data: Vec<u8>,
    data_buffer: &mut Vec<u8>,
) -> Result<(), SaveError> {
    zlib::Decoder::new(Cursor::new(&data)).read_to_end(data_buffer)?;
    Ok(())
}

fn get_packets<T>(
    value: &mut T,
    version: i32,
) -> Result<Vec<Vec<u8>>, SaveError>
where
    T: Seek + Read + Clone,
{
    info!("Getting packets");
    let mut data_packets: Vec<Vec<u8>> = Vec::new();

    let prev_pos = value.stream_position()?;
    value.seek(SeekFrom::End(0))?;
    let end_pos = value.stream_position()?;
    value.seek(SeekFrom::Start(prev_pos))?;

    while value.stream_position()? < end_pos {
        assert_eq!(value.read_u32::<LittleEndian>()?, PACKET_SIGNATURE); // Signature
        value.read_i32::<LittleEndian>()?; // Padding
        assert_eq!(value.read_i32::<LittleEndian>()?, 128 * 1024); //Assert Max Chunk Size
        value.read_i32::<LittleEndian>()?; // Padding
        if version >= 41 {
            value.seek(SeekFrom::Current(1))?; // Padding?
        }
        let compressed_size = value.read_i32::<LittleEndian>()?;
        value.read_i32::<LittleEndian>()?; // Padding
        let uncompressed_size = value.read_i32::<LittleEndian>()?; // Uncompressed Size
        value.read_i32::<LittleEndian>()?; // Padding
        assert_eq!(value.read_i32::<LittleEndian>()?, compressed_size); // Assert Compressed Size
        value.read_i32::<LittleEndian>()?; // Padding
        assert_eq!(value.read_i32::<LittleEndian>()?, uncompressed_size); // Assert Uncompressed Size

        value.read_i32::<LittleEndian>()?; // Padding
        debug!("Reading packet of length: {compressed_size:X}");

        let data = read_bytes(value, compressed_size as u64)?;

        data_packets.push(data);
    }

    Ok(data_packets)
}

type DecompressionHandle = JoinHandle<Result<(u32, Vec<u8>), SaveError>>;

fn decompress_multithreaded(
    data_packets: Vec<Vec<u8>>,
) -> Result<Vec<u8>, SaveError> {
    let mut handles: Vec<DecompressionHandle> = Vec::new();

    let available_cpus = available_parallelism()?.get();

    for (index, packet) in data_packets
        .chunks(data_packets.len() / available_cpus)
        .map(ToOwned::to_owned)
        .enumerate()
    {
        handles.push(thread::spawn(move || {
            let mut data_buffer: Vec<u8> = Vec::new();

            for data in packet {
                decompress_chunk(data, &mut data_buffer)?;
            }

            Ok((index as u32, data_buffer))
        }));
    }

    let mut decompressed_chunks: Vec<(u32, Vec<u8>)> = Vec::new();

    for handle in handles {
        let result = handle.join().expect("Decompression thread failed")?;
        decompressed_chunks.push(result);
    }
    info!("Done inflating chunks!");

    info!("Sorting chunks...");
    decompressed_chunks.sort_unstable_by_key(|a| a.0);

    info!("Merging chunks...");

    let mut merged_chunks: Vec<u8> = Vec::new();

    for mut chunk in decompressed_chunks {
        merged_chunks.append(&mut chunk.1);
    }

    info!("Finished decompression!");
    Ok(merged_chunks)
}

pub fn decompress_body<T>(
    value: &mut T,
    header: &SaveHeader,
) -> Result<Vec<u8>, SaveError>
where
    T: Seek + Read + Clone,
{
    let data_packets = get_packets(value, header.save_version)?;

    info!("Inflating {} packets...", data_packets.len());

    decompress_multithreaded(data_packets)
}
