use thiserror::Error;

#[derive(Error, Debug)]
pub enum SaveError {
    #[error(transparent)]
    IO(#[from] std::io::Error),
    #[error(transparent)]
    Serde(#[from] serde_json::Error),
    #[error("{0:?}")]
    Parsing(&'static str),
}
