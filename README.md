# Satisfactory-Save
![Drone (self-hosted)](https://img.shields.io/drone/build/miaapancake/satisfactory-save?server=https%3A%2F%2Fci.miadoes.dev&style=for-the-badge&logo=drone)
![Static Badge](https://img.shields.io/badge/License-LGPL_V3.0-C577F9?style=for-the-badge)

A savefile parsing library for [Satisfactory](https://www.satisfactorygame.com/)
written in Rust.
